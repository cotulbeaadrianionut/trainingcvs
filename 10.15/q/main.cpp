#include <iostream>
#include <string>

std::string getName(){
    std::string x;
    std::cout<<"Enter a name: ";
    std::cin>>(x);

    return x;
}

int main()
{
    std::string names[]{"Alex","Betty","Caroline","Dave","Emily","Fred","Greg","Holly"};

    std::string name{getName()};

    bool found{false};

    for(auto x:names)
        if(x==name)
        {
        found=true;
        break;
        }

    if(found)
        std::cout<<name<<" was found. \n";
    else
        std::cout<<name<<" was not found. \n";

    return 0;
}
