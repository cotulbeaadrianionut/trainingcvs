#include <iostream>

void positiveDecToBin(int x){
    if(x==0)
        return;

    positiveDecToBin(x/2);
    std::cout<<x%2;
}

void decToBin(unsigned int x){
    if(x>1)
        decToBin(x/2);
    std::cout<<x%2;
}

int main()
{
    int x;
    std::cin>>x;
    std::cout<<std::endl;
    positiveDecToBin(x);

    std::cin>>x;
    std::cout<<std::endl;
    decToBin(static_cast<unsigned int>(x));

    return 0;
}
