#include <iostream>

using namespace std;

int sumOfDigits(int n){
    if(n==0)
        return 0;
    else
        return sumOfDigits(n/10)+n%10;
}

int main()
{
    int x;
    cin>>x;
    cout<<'\n'<<sumOfDigits(x);
    return 0;
}
