#include <iostream>

#include "io.h"

using namespace std;

int main()
{
    int a{},b{};
    a=readNumber();
    b=readNumber();

    writeAnswer(a,b);

    return 0;
}
