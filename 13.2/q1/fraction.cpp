#include "fraction.h"
#include <iostream>

Fraction::Fraction(int numerator,int denominator)
    :m_numerator{numerator},m_denominator{denominator}
    {
    }

void Fraction::print() const{
    std::cout<<m_numerator<<"/"<<m_denominator<<"\n";
}

Fraction operator*(const Fraction& fr, int x){
    return {fr.m_numerator*x,fr.m_denominator};
}

Fraction operator*(int x, const Fraction& fr){
    return {fr.m_numerator*x,fr.m_denominator};
}

Fraction operator*(const Fraction& x,const Fraction& y){
    return {x.m_numerator*y.m_numerator , y.m_denominator*x.m_denominator};
}
