#ifndef FRACTION_H
#define FRACTION_H

class Fraction{
    private:
    int m_numerator{};
    int m_denominator{1};

    public:
    Fraction(int numerator, int denominator=1);
    void print() const;

    friend Fraction operator*(const Fraction& fr,int x);
    friend Fraction operator*(int x, const Fraction& fr);
    friend Fraction operator*(const Fraction& x, const Fraction& y);
};

#endif
