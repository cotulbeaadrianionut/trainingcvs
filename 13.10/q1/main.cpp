#include <iostream>
#include <string>
#include <cassert>

class Mystring
{
    private:
        std::string m_string{};
    public:
        Mystring(const std::string& string = {})
        :   m_string{string}
        {
        }

        std::string operator()(int start,int length)
        {
            assert(start>=0);
            assert(start+length <= static_cast<int>(m_string.length()));

            std::string sub{};
            for(int i{0};i<length;++i)
            {
                sub +=m_string[static_cast<std::string::size_type>(start+i)];
            }
            return sub;
        }
};

int main()
{
	Mystring string{ "Hello, world!" };
	std::cout << string(7, 5) << '\n'; // start at index 7 and return 5 characters

	return 0;
}
