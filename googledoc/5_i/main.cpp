#include <iostream>
#include <pthread.h>
#include <thread>
#include <mutex>

std::mutex mtx;

void print(char c){
//	std::lock_guard<std::mutex> lock(mtx);
	for(int i=0;i<100;i++){
//	std::lock_guard<std::mutex> lock(mtx);
	std::cout<<c<<" \n";
	}
}

int main(){
std::thread a (print,'A');
std::thread b (print,'B');
std::thread c (print,'C');

a.join();
b.join();
c.join();

return 0;

}
