#include <iostream>
#include <string>

class Animal{
	virtual void eat()=0;
};

/* class Dog : public Animal{
	public:
	Dog(int n): Animal{4}{}
	void bark(){ std::cout<<"Bark!!"<<std::endl;}
}; */

class Cat : public Animal{
	private:
	int m_legNum;
	public:
	Cat(int n): m_legNum{n}{}

	void eat()override{
	std::cout<<"Cat eats"<<std::endl;
	}
};

int main(){

	Cat pisi{4};
	pisi.eat();

	return 0;
};
