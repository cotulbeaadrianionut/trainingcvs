#include <iostream>

void increment(int * n){
	(*n)++;
}

void increment_referrence(int &n){
	n++;
	}


int main(){
	int a=5;
	int * x=&a ;
	increment(x);
	std::cout<<*x<<std::endl;

	return 0;
}
