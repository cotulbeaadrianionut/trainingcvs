#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
#include <sstream>


std::string toBinary(long int x)
{
	char bin32[]  = "00000000000000000000000000000000";
	for(int i=31;i>=0;i--)
	{
		if(x%2)
		{
			bin32[i]='1';
		}
		x/=2;
	}

	std::stringstream ss;
	ss<<bin32;
	std::string tmp;
	ss>>tmp;


	return tmp;
}

long int fromBinary(std::string x)
{
	long int res=0,tmp;
	int k;

	for(int i=31;i>=0;i--)
	{

		if(x[i]=='1')
		{
			k=0;
			tmp=1;
			while(k<(31-i))
			{
				tmp*=2;
				k++;
			}
			res+=tmp;
		}
	}
	return res;
}


void write()
{
	std::string final="";
	std::string message;
	getline(std::cin,message);
	time_t t1 = time(0);
	final+="6";

	int itmp=message.size();
	final= final + toBinary(t1)+ toBinary(itmp) +"6" +message;

	std::ofstream output("out.txt");
	if(output.is_open())
	{
		output<<final;
	}
	else
	{
	std::cout<<"Unable to open out.txt for output"<<std::endl;
	exit(1);
	}
	output.close();

	std::cout<<std::endl<<std::endl<<final<<std::endl<<std::endl;
}

void read()
{
	std::string final="";
	std::ifstream input("out.txt");
	if(input.is_open())
	{
		char firstsep;
		input.get(firstsep);

		char timestr[33];
		input.get(timestr,33);
		char sizestr[33];
		input.get(sizestr,33);

		std::stringstream ss;

		ss<<timestr;
		std::string tmp;
		ss>>tmp;
		time_t t=fromBinary(tmp);
		tm* timestruct = localtime(&t);
		tmp=asctime(timestruct);
		tmp.erase(tmp.size()-1,1);
		final+=tmp;

		std::stringstream ss2;
		ss2<<sizestr;
		std::string stmp;
		ss2>>stmp;
		long int size=fromBinary(stmp);

		char lastsep;
		input.get(lastsep);

		char message[100];
		input.get(message,size+1);
		final= final+" "+message;

		std::cout<<std::endl<<final<<std::endl;

	}
	else
	{
	std::cout<<"Unable to open out.txt for input"<<std::endl;
	exit(1);
	}
	input.close();
}

int main()
{
	write();
	write();
	read();
	read();
}





