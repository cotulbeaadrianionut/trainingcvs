#include <iostream>

void a(int n){
	for(int i=0;i<n;i++)
	std::cout<<'a';
	std::cout<<std::endl;
}

void b(int n){
	for(int i=0;i<n;i++)
	std::cout<<'b';
	std::cout<<std::endl;
}

void run(void (*fun)(int n)){
	int i=0;
	while(i<5)
	{(*fun)(i+1);
	i++;
	}
}
int main(){

	run(a);
	run(b);

	return 0;
}
