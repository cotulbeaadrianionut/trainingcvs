#include <iostream>
#include <vector>

template <class T>
class MyListItem
{
	private:
	T m_data;
	MyListItem<T> * m_next;

	public:
	MyListItem():m_data{0},m_next{NULL}
	{}

	MyListItem(T data)
	{
	m_data=data;
	m_next=NULL;
	}

	void setNext(MyListItem<T> * next){
	m_next=next;
	}

	T getData(){
		return m_data;
	}

	MyListItem<T> * getNext(){
		return m_next;
	}
};

template <class T>
class MyList
{
	private:
	int count;
	std::vector<MyListItem<T> > list;

	public:
		MyList()
	{
		count=0;
	}

	void add(T data)
	{
		count++;
		list.resize(count);
		MyListItem<T> ob{data};

		list[count-1]=ob;

		if(count>1)
		{
			list[count-1].setNext(&ob);
		}

		ob.setNext(NULL);
	}

	void print(){

		for(int i =0 ; i < count ; i++)
		{
			std::cout<<list[i].getData()<<" "<<list[i].getNext()<<std::endl;
		}
	}
};

int main(){
	MyList<int> list;
	list.add(25);
	list.add(23);
	list.print();
}
