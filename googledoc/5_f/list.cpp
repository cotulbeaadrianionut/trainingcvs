#include <iostream>
#include <list>
#include <iterator>

template <class T>
class MyListItem
{
	private:
	T m_data;
	MyListItem<T> * m_next;

	public:
	MyListItem():m_data{0},m_next{NULL}
	{}

	MyListItem(T data)
	{
	m_data=data;
	m_next=NULL;
	}

	void setNext(MyListItem<T> * next){
	m_next=next;
	}

	T getData(){
		return m_data;
	}

	MyListItem<T> * getNext(){
		return m_next;
	}
};

template <class T>
class MyList
{
	private:
	std::list<MyListItem<T> > list;
	public:
		MyList()
	{
	}

	void add(T data)
	{
		MyListItem<T> ob{data};
		if(list.empty())
		{
			list.push_back(ob);
		}
		else
		{
			list.back().setNext(&ob);
			list.push_back(ob);
		}
	}

	void print(){

		for(typename std::list<MyListItem<T>>::iterator it = list.begin() ; it != list.end(); it++)
		{
			std::cout<<' '<<it->getData();
		}
		std::cout<<std::endl;
	}
};

int main(){
	MyList<int> list;
	list.add(25);
	list.add(23);
	list.print();

	return 0;
}
