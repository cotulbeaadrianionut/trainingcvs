#include <iostream>
#include <string>

class Animal{
	private:
	int m_legNum;
	public:
		Animal(int n): m_legNum{n} {}
		
		void eat(){
		std::cout<<"Animal eats"<<std::endl;
		}
};

class Dog : public Animal{
	public:
	Dog(int n): Animal{4}{}
	void bark(){ std::cout<<"Bark!!"<<std::endl;}
};

int main(){

	Animal anim{4};
	anim.eat();
	Dog doggy{4};
	doggy.bark();
};
