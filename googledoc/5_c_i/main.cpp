#include <iostream>

void increment(int * n){
	(*n)++;
	std::cout<<n<<std::endl;
	std::cout<<&n<<std::endl;
}

int main(){

	int x = 5;

	increment(&x);
	std::cout<<&x<<std::endl;

	return 0;
}
