#include <iostream>

void a(){
	for(int i=0;i<10;i++)
	std::cout<<'a';
	std::cout<<std::endl;
}

void b(){
	for(int i=0;i<10;i++)
	std::cout<<'b';
	std::cout<<std::endl;
}

void run(void (*fun)()){
	int i=0;
	while(i<5)
	{(*fun)();
	i++;
	}
}
int main(){

	run(a);
	run(b);

	return 0;
}
