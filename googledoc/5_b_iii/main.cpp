#include <iostream>
#include <string>
#include <vector>

class Animal{
	virtual void eat()=0;
};

class Dog : public Animal{
        private:
        int m_legNum;
        public:
        Dog(int n): m_legNum{n}{}

	void barks(){
	std::cout<<"Bark!!!"<<std::endl;}

        void eat()override{
        std::cout<<"Dog eats"<<std::endl;
        }
};


class Cat : public Animal{
	private:
	int m_legNum;
	public:
	Cat(int n): m_legNum{n}{}

	void eat()override{
	std::cout<<"Cat eats"<<std::endl;
	}
};

int main(){

        Dog *cutu=new Dog{4};
	Cat *pisi=new Cat{4};
	std::vector<Animal *> arr;
	arr.push_back(cutu);
	arr.push_back(pisi);
	arr[1]->eat();
	return 0;
};
