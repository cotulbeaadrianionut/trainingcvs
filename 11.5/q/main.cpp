#include <iostream>

int sumTo(int x){
    int i{1},s{0};
    for(i<=x)
        s+=i;
    return s;
}

void printEmployeeName(const Employee& employee){
    std::cout<<employee.name;
}

std::pair<int,int> minmax(int x,int y);

int getIndexOfLargestValue(const std::vector<int>& array );

const std::string& getElement(const std::vector<std::string>& array,int index);

int main()
{
    cout << "Hello world!" << endl;
    return 0;
}
