#include <iostream>
#include <cstddef>
#include <algorithm>

int main()
{
    std::cout<<"How many names you want to enter?\n";
    std::size_t many{};
    std::cin>>many;

    auto *array{new std::string[many]};

    for(int index{0}; index<many;index++)
    {
        std::cout<<"Enter name #"<<index+1<<": ";
        std::cin>>array[index];
    }

    std::sort(array,array+many);

    for(int index{0}; index<many;index++)
    {
        std::cout<<"Name #"<<index+1<<": "<<array[index]<<std::endl;
    }

    delete[] array;
    array=0;
}
