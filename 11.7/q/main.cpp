#include <iostream>
#include <functional>
#include <cstdlib>

int getInteger(){
    int x;
    std::cout<<"Int input:";
    std::cin>>x;
    return x;
}

char getOp(){
    std::cout<<"Enter a mathematical operation: ";
    char c{};
    std::cin>>c;
    while(c!='+' && c!='-' && c!='*' && c!='/')
        std::cin>>c;
    return c;
}

int add(int x, int y){
    return x+y;
}

int substract(int x,int y){
    return x-y;
}

int multiply(int x,int y){
    return x*y;
}

int divide(int x,int y){
    return x/y;
}

using ArithmeticFuction=std::function<int(int,int)>;

ArithmeticFuction getArithmetic(char op){
    if(op=='+')
    return &add;
    if(op=='-')
    return &substract;
    if(op=='*')
    return &multiply;
    if(op=='/')
    return &divide;
}

int main()
{   int x{getInteger()};
    char op{getOp()};
    int y{getInteger()};


    ArithmeticFuction f{getArithmetic(op)};
    std::cout<<x<<' '<<op<<' '<<y<<" = "<< f(x,y)<<'\n';

    return 0;

}
