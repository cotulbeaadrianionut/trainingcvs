#include <iostream>
#include <iterator>
#include <limits>

int main()
{
    constexpr int array[] {4,6,7,3,8,2,1,9,5};

    int x{};
    do{
    std::cin>>x;
    if(std::cin.fail())
        std::cin.clear();

    std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }while(x<1||x>9);

    for(int i{0};i<sizeof(array)/sizeof(array[0]);i++)
        std::cout<<array[i]<<std::endl;

    for(int i{0}; i<sizeof(array)/sizeof(array[0]); i++)
    {
        if(array[i]==x)
        {std::cout<<std::endl<<"x's index is "<<i;
        break;
        }
    }
    return 0;
}
