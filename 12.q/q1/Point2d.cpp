#include "Point2d.h"
#include <iostream>
#include <cmath>
Point2d::Point2d(double x,double y)
    : m_x{x},m_y{y}
    {
    }


void Point2d::print() const
{
    std::cout<<"Point2d("<<m_x<<", "<<m_y<<")\n";
}

double distanceTo(const Point2d& a, const Point2d& b)
{
		return std::sqrt((a.m_x - b.m_x) * (a.m_x - b.m_x) + (a.m_y - b.m_y) * (a.m_y - b.m_y));
}
