#ifndef POINT2D_H
#define POINT2D_H

class Point2d
{
    private:
        double m_x{};
        double m_y{};

    public:

    Point2d(double x=0, double y=0);
    void print() const;
    friend double distanceTo(const Point2d& a,const Point2d& b);
};

#endif // POINT2D_H
