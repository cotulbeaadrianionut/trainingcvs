#include "MonsterGenerator.h"
#include <cstdlib>
#include <array>
#include <iostream>


 int MonsterGenerator::getRandomNumber(int min, int max)
    {
        static constexpr double fraction{1.0 /(RAND_MAX +1.0)};

        return min+ static_cast<int>((max-min+1)*(std::rand()*fraction));
    }

Monster MonsterGenerator::generateMonster()
    {
        auto type{ static_cast<Monster::MonsterType>(getRandomNumber(0,static_cast<int>(Monster::MonsterType::max_monster_types)-1))};

        static constexpr std::array<const char*,6> s_names{"Blarg","Moog","Pksh","Tyrn","Mort","Hans"};
        static constexpr std::array<const char*,6>  s_roars{ "*ROAR*", "*peep*", "*squeal*", "*whine*", "*hum*", "*burp*"};

        auto name{s_names[getRandomNumber(0,static_cast<int>(s_names.size()-1))]};
        auto roar{s_roars [getRandomNumber(0,static_cast<int>(s_roars.size()-1))]};

        int hp(getRandomNumber(0,100));

       return {type, name, roar,hp};

    }
