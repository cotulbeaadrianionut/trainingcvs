#ifndef MONSTER_H
#define MONSTER_H
#include <string>
class Monster
{
    public:
    enum class MonsterType{
        Dragon,
        Goblin,
        Ogre,
        Orc,
        Skeleton,
        Troll,
        Vampire,
        Zombie,

        max_monster_types
    };

    private:
        MonsterType m_type{};
        std::string m_name{};
        std::string m_roar{};
        int m_hp{};

    public:
        Monster(Monster::MonsterType type, const std::string& name, const std::string& roar,int hp);
        std::string getMonsterTypeString() const;

        void print() const;

};

#endif
