#include "Monster.h"
#include <iostream>
#include <string>

Monster::Monster(Monster::MonsterType type, const std::string& name, const std::string& roar,int hp)
        :
          m_type{type},
          m_name{name},
          m_roar{roar},
          m_hp{hp}
          {}

std::string  Monster::getMonsterTypeString() const{
            switch(m_type){
            case Monster::MonsterType::Dragon:   return "dragon";
            case Monster::MonsterType::Goblin:   return "goblin";
            case Monster::MonsterType::Ogre:     return "ogre";
            case Monster::MonsterType::Orc:      return "orc";
            case Monster::MonsterType::Skeleton: return "skeleton";
            case Monster::MonsterType::Troll:    return "troll";
            case Monster::MonsterType::Vampire:  return "vampire";
            case Monster::MonsterType::Zombie:   return "zombie";
            default: return "???";
            }
        }
void Monster::print() const
        {
            std::cout<<m_name<<" the "<<getMonsterTypeString()<<" has "<<m_hp<<" hit point and yells "<<m_roar<<'\n';
        }
