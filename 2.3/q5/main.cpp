#include <iostream>

using namespace std;

int doubleNumber(int n)
{
    return 2*n;
}

int getValue()
{
    int x;
    cin>>x;
    return x;
}

void printRes(int y)
{
    cout<<y<<" is the res";
}

int main()
{
    printRes(doubleNumber(getValue()));
    return 0;
}
