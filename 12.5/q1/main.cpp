#include <iostream>
#include <string>
class Ball
{
    private:
        std::string m_color{"black"};
        double radius{10.0};

    public:
        Ball(std::string cpy, double rad)
        {
            m_color=cpy;
            radius=rad;
        }

        Ball(std::string cpy)
        {
            m_color=cpy;
        }

        Ball(double rad)
        {
            radius=rad;
        }

        Ball() {};

        void print()
        {
        std::cout<<"color: "<<m_color<<"  radius: "<<radius<<"\n";
        }

};

int main()
{
	Ball def{};
	def.print();

	Ball blue{ "blue" };
	blue.print();

	Ball twenty{ 20.0 };
	twenty.print();

	Ball blueTwenty{ "blue", 20.0 };
	blueTwenty.print();

	return 0;
}
