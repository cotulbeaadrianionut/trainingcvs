#include <iostream>
#include <vector>
#include <algorithm>

struct student{
    std::string name{};
    int grade{};
};

int getStudN()
{
    int number{};
    while(!number)
    {
        std::cout<<"Enter how many students: ";
        std::cin>>number;
    }
    return number;
}

std::vector<student> getStuds()
{
    using vector_type = std::vector<student>;

    int StudsN{getStudN()};

    vector_type students(static_cast<vector_type::size_type>(StudsN));

   int stud {1};

   for(auto& student: students)
   {
    std::cout<<"Enter name #"<<stud<<": ";
    std::cin>>student.name;
    std::cout<<"Enter grade #"<<stud<<": ";
    std::cin>>student.grade;

    ++stud;
   }
   return students;
}

bool compareStudents(const student& a, const student& b)
{
  return (a.grade > b.grade);
}

int main()
{
    auto students{getStuds()};
    std::sort (students.begin(), students.end(),compareStudents);

    for(auto& stud: students)
    {
        std::cout<<stud.name << " got a grade of "<<stud.grade<<'\n';
    }

    return 0;
}
