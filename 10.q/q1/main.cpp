#include <iostream>
#include <array>
#include <numeric>

enum Items{
    health_pot,
    torches,
    arrows,
    max_items
};

using inventory_type = std::array<int , Items::max_items>;

int countTotalItems(const inventory_type& items)
{
      return std::accumulate(items.begin(), items.end(),0);
}

int main()
{
    inventory_type inv{2,5,10};

    std::cout<<"The player has "<<countTotalItems(inv)<<" item(s) in total.\n";

    std::cout<<"The player has "<<inv[Items::torches]<<" torches";

    return 0;
}
