#include <iostream>
#include <array>
#include <ctime>
#include <random>
#include <algorithm>


enum class Suits
{
    suit_club,
    suit_diamond,
    suit_heart,
    suit_spade,

    max_suits
};

enum class Ranks{
    rank_2,
    rank_3,
    rank_4,
    rank_5,
    rank_6,
    rank_7,
    rank_8,
    rank_9,
    rank_10,
    rank_Jack,
    rank_Queen,
    rank_King,
    rank_Ace,

    max_ranks
};

struct Card{
    Suits suit{};
    Ranks rank{};
};

using deck_type = std::array<Card,52>;
using index_type = deck_type::size_type;

void printCard(const Card& c){
    switch(c.rank)
    {
    case Ranks::rank_2 : std::cout<<'2'; break;
    case Ranks::rank_3 : std::cout<<'3'; break;
    case Ranks::rank_4 : std::cout<<'4'; break;
    case Ranks::rank_5 : std::cout<<'5'; break;
    case Ranks::rank_6 : std::cout<<'6'; break;
    case Ranks::rank_7 : std::cout<<'7'; break;
    case Ranks::rank_8 : std::cout<<'8'; break;
    case Ranks::rank_9 : std::cout<<'9'; break;
    case Ranks::rank_10: std::cout<<'T';break;
    case Ranks::rank_Jack: std::cout<<'J'; break;
    case Ranks::rank_Queen: std::cout<<'Q'; break;
    case Ranks::rank_King: std::cout<<'K'; break;
    case Ranks::rank_Ace: std::cout<<'A'; break;
    default:
        std::cout<<"?";
        break;
    }

    switch (c.suit)
    {
    case Suits::suit_club:       std::cout << 'C';   break;
    case Suits::suit_diamond:    std::cout << 'D';   break;
    case Suits::suit_heart:      std::cout << 'H';   break;
    case Suits::suit_spade:      std::cout << 'S';   break;
    default:
        std::cout << '?';
        break;
    }

}

deck_type createDeck()
{
    deck_type deck{};
    index_type card{0};

    auto suitss{static_cast<int>(Suits::max_suits)};
    auto rankss{static_cast<int>(Ranks::max_ranks)};

    for(int suit{0}; suit<suitss;++suit)
    {
        for(int rank{0}; rank <rankss;++rank)
           { deck[card].suit=static_cast<Suits>(suit);
            deck[card].rank=static_cast<Ranks>(rank);
            ++card;
            }
    }
    return deck;
}

void printDeck(const deck_type& deck)
{
    for(const auto& card:deck)
    {
        printCard(card);
        std::cout<<' ';
    }
    std::cout<<'\n';
}

void shuffleDeck(deck_type& deck)
{
    static std::mt19937 mt{static_cast<std::mt19937::result_type>(std::time(nullptr))};

    std::shuffle(deck.begin(),deck.end(),mt);
}

int main()
{
    auto deck{ createDeck()};
    printDeck(deck);
    shuffleDeck(deck);
    std::cout<<'\n';
    printDeck(deck);
    return 0;
}
