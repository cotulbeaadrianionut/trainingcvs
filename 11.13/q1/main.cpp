#include <iostream>
#include <array>
#include <algorithm>
#include <string>

struct Student{
        std::string name{};
        int points{};
};


int main()
{
    const std::array<Student, 8> arr{
    { { "Albert", 3 },
      { "Ben", 5 },
      { "Christine", 2 },
      { "Dan", 8 },
      { "Enchilada", 4 },
      { "Francis", 1 },
      { "Greg", 3 },
      { "Hagrid", 5 } }
  };

    const auto maxi{
        std::max_element(arr.begin(), arr.end(), [](auto &x, auto&y){
        return(x.points<y.points); })
    };

    std::cout<<maxi->name;

    return 0;
}
